package com.example.uberusermicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UberUserMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UberUserMicroserviceApplication.class, args);
	}

}
