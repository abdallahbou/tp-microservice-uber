package com.example.uberusermicroservice.controller;

import com.example.uberusermicroservice.dto.RequestPassengerDTO;
import com.example.uberusermicroservice.dto.ResponsePassengerDto;
import com.example.uberusermicroservice.exception.NotFoundException;
import com.example.uberusermicroservice.services.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
@CrossOrigin(value = "http://localhost:8080", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class PassengerController {

    @Autowired
    private PassengerService _passengerService;

    @PostMapping("/passenger")
    public ResponseEntity<ResponsePassengerDto> post(@RequestBody RequestPassengerDTO passengerDTO) throws NotFoundException {
        try {


            if (_passengerService.findPassengerByEmail(passengerDTO.getEmail()) != null) {
                ResponsePassengerDto dto = new ResponsePassengerDto();
                dto.setMessage("Passenger already exist");
                return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
//                return ResponseEntity.notFound().build();
            }
        } catch (NotFoundException ex) {

            return ResponseEntity.ok(_passengerService.createPassenger(passengerDTO));

        }
        return null;
    }

//        @GetMapping("search/{search}")
//        public ResponseEntity<ResponsePassengerDto> get (@PathVariable String search){
//            try {
//                return ResponseEntity.ok(_passengerService.getPassenger(search + "%"));
//            } catch (NotFoundException ex) {
//                return ResponseEntity.notFound().build();
//            }
//        }

    @GetMapping("search/{email}")
    public ResponseEntity<ResponsePassengerDto> get(@PathVariable String email) {
        try {
            return ResponseEntity.ok(_passengerService.findPassengerByEmail(email));
        } catch (NotFoundException e) {
            ResponsePassengerDto dto = new ResponsePassengerDto();
            dto.setMessage("Passenger doesn't exist");
            return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
        }
    }


}
