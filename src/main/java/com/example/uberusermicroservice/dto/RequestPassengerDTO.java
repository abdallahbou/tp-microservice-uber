package com.example.uberusermicroservice.dto;

import lombok.Data;

@Data
public class RequestPassengerDTO {

    private String email;

}
