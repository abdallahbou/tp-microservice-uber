package com.example.uberusermicroservice.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponsePassengerDto {
    private int id;

    private String email;

    private String message;





}
