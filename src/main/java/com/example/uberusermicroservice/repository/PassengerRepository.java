package com.example.uberusermicroservice.repository;

import com.example.uberusermicroservice.entity.Passenger;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PassengerRepository extends CrudRepository<Passenger, Integer> {

    @Query("SELECT c from Passenger c where c.email like :search")
    public Passenger searchPassenger(@Param("search") String search);

    public Passenger findPassengerByEmail(String email);
}
