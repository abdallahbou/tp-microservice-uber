package com.example.uberusermicroservice.services;

import com.example.uberusermicroservice.dto.RequestPassengerDTO;
import com.example.uberusermicroservice.dto.ResponsePassengerDto;
import com.example.uberusermicroservice.exception.NotFoundException;

public interface PassengerService {

    public ResponsePassengerDto createPassenger(RequestPassengerDTO passengerDTO);

    public ResponsePassengerDto getPassenger(String search) throws NotFoundException;

    public ResponsePassengerDto findPassengerByEmail(String email) throws NotFoundException;
}
