package com.example.uberusermicroservice.services;

import com.example.uberusermicroservice.dto.RequestPassengerDTO;
import com.example.uberusermicroservice.dto.ResponsePassengerDto;
import com.example.uberusermicroservice.entity.Passenger;
import com.example.uberusermicroservice.exception.NotFoundException;
import com.example.uberusermicroservice.repository.PassengerRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PassengerServiceImpl implements PassengerService{

    @Autowired
    private PassengerRepository _passengerRepository;

    @Autowired
    private ModelMapper _modelMapper;

    @Override
    public ResponsePassengerDto createPassenger(RequestPassengerDTO passengerDTO) {

        return _modelMapper.map(_passengerRepository.save(_modelMapper.map(passengerDTO, Passenger.class)),ResponsePassengerDto.class);
    }

    @Override
    public ResponsePassengerDto getPassenger(String search) throws NotFoundException {
        Passenger passenger = _passengerRepository.searchPassenger(search);
        if(passenger == null){
            throw new NotFoundException();
        }
        return _modelMapper.map(passenger, ResponsePassengerDto.class);
    }

    @Override
    public ResponsePassengerDto findPassengerByEmail(String email) throws NotFoundException {
        Passenger passenger= _passengerRepository.findPassengerByEmail(email);
        if(passenger == null ){
            throw new NotFoundException();
        }

        return _modelMapper.map(passenger,ResponsePassengerDto.class);
    }
}
